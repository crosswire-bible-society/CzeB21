MOD_DIR=modules/texts/ztext/czeb21
MODULES=$(MOD_DIR)/nt $(MOD_DIR)/nt.vss $(MOD_DIR)/ot $(MOD_DIR)/ot.vss
#OSIS2MOD=/home/matej/archiv/knihovna/repos/sword/utilities/osis2mod
OSIS2MOD=osis2mod
V11N=Luther

all: module

$(MODULES): CzeB21.xml
	mkdir -p $(MOD_DIR)
	$(OSIS2MOD) $(MOD_DIR)  $< -v $(V11N) -z z

module: $(MODULES) czeb21.conf
	install -D -m 644 czeb21.conf mods.d/czeb21.conf
	sed -i -e 's/@V11N@/$(V11N)/' mods.d/czeb21.conf
	zip -9vT CzeB21.zip $(MOD_DIR)/* mods.d/*

zip2crosswire: $(MODULES) czeb21.conf
	zip -9vT CzeB21.zip CzeB21.xml czeb21.conf

validate: CzeB21.xml
	xmllint --noout --schema OSIS-standard/osisCore.2.1.1.xsd $<

prettyPrint: CzeB21.xml
	tidy -m -i -xml -utf8 --quote-nbsp no $<
	# Because echo -ne '\u00A0'|xxd
	# sed -i -e 's/&#160;/\xc2\xa0/g' $<
	sed -i -e 's/\s+$$//' $<

clean:
	rm -f *~ $(MODULES) CzeB21.zip

.PHONY: prettyPrint validate zip2crosswire clean
